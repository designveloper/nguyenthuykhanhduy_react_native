/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView
} from 'react-native';

import logo from './assets/1.jpg';
import search from './assets/search.png';
import element from './assets/7.jpg';
import heart from './assets/heart.png';
// import logo from './images/StoriesHeaderThumbnails/1.jpg';

export default class App extends Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.header}>
          <Image style={styles.logo} source={logo} />
          <TextInput style={{height: 40, width: 250}} placeholder="Type here to translate!" />
          <Image style={styles.search} source={search} />
        </View>
        <View style={{height: 120, marginLeft: 10}}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.column}>
              <Image
                style={styles.icon}
                source={logo}
              />
              <Text> Hello world! </Text>
            </View>
            <View style={styles.column}>
              <Image
                style={styles.icon}
                source={logo}
              />
              <Text> UI8 </Text>
            </View>
            <View style={styles.column}>
              <Image
                style={styles.icon}
                source={logo}
              />
              <Text> Unfold </Text>
            </View>
            <View style={styles.column}>
              <Image
                style={styles.icon}
                source={logo}
              />
              <Text> Netguru </Text>
            </View>
            <View style={styles.column}>
              <Image
                style={styles.icon}
                source={logo}
              />
              <Text> Text </Text>
            </View>
            <View style={styles.column}>
              <Image
                style={styles.icon}
                source={logo}
              />
              <Text> Ola </Text>
            </View>
            <View style={styles.column}>
              <Image
                style={styles.icon}
                source={logo}
              />
              <Text> Hello world! </Text>
            </View>
          </ScrollView>
        </View>
        <View style={styles.element}>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Image source={element} style={styles.elementImage} />
          </View>
          
          <View style={styles.elementCaption}>
            <Image style={{flex: 0.2}} source={logo} style={styles.icon}></Image>
            <View style={{flex: 0.9, marginLeft: 20, justifyContent: 'center'}}>
              <Text style={{fontSize: 15, fontWeight: 'bold'}}>Alexandra Zutto</Text>
              <Text>3 minutes ago</Text>
            </View>
            <View style={styles.icon1}>
              <Image style={styles.icon1Image} source={heart} ></Image>
            </View>
          </View>
        </View>

        <View style={styles.element}>
          <ScrollView horizontal={true}>
            <View style={{margin: 10}}><Image source={element} style={styles.elementImage1} /></View>
            <View style={{margin: 10}}><Image source={element} style={styles.elementImage1} /></View>
            <View style={{margin: 10}}><Image source={element} style={styles.elementImage1} /></View>
            <View style={{margin: 10}}><Image source={element} style={styles.elementImage1} /></View>
            <View style={{margin: 10}}><Image source={element} style={styles.elementImage1} /></View>
            <View style={{margin: 10}}><Image source={element} style={styles.elementImage1} /></View>
          </ScrollView>
          
          <View style={styles.elementCaption}>
            <Image style={{flex: 0.2}} source={logo} style={styles.icon}></Image>
            <View style={{flex: 0.9, marginLeft: 20, justifyContent: 'center'}}>
              <Text style={{fontSize: 15, fontWeight: 'bold'}}>Alexandra Zutto</Text>
              <Text>3 minutes ago</Text>
            </View>
            <View style={styles.icon1}>
              <Image style={styles.icon1Image} source={heart} ></Image>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    marginTop: 20,
    marginLeft: 20,
    flexDirection: 'row'
  },
  search: {
    width: 30,
    height: 30,
    marginTop: 5
  },
  logo: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 20
  },
  column: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center',
    height: 150,
    marginRight: 20
  },
  icon: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  icon1: {
    flexDirection: 'column',
    justifyContent: 'center'
  },
  icon1Image: {
    width: 30,
    height: 30,
  },
  element: {
    marginTop: 20,
    flex: 1,
  },
  elementImage: {
    width: 380,
    height: 220
  },
  elementImage1: {
    width: 320,
    height: 220
  },
  elementCaption: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20
  }
});
